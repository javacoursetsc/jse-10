package ru.arubtsova.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

}
