package ru.arubtsova.tm.api.controller;

public interface ITaskController {

    void showList();

    void create();

    void clear();

}
