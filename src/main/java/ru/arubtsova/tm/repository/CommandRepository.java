package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.constant.ArgumentConst;
import ru.arubtsova.tm.constant.TerminalConst;
import ru.arubtsova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "show application version."
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "show terminal commands."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "show system information."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "close application."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null, "create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null, "delete all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null, "show all tasks."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null, "create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null, "delete all projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null, "show all projects."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "show application arguments."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO, EXIT, COMMANDS, ARGUMENTS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
